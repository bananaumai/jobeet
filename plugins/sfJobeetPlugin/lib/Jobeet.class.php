<?php

class Jobeet
{
  static public function slugify($text)
  {
    // 文字ではないもしくは数値を-に置き換える
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    $text = trim($text, '-');

    if (function_exists('iconv')) {
      //$text = iconv('utf-8', 'us-ascii/TRANSLIT', $text);
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    }

    $text = strtolower($text);

    // 望まない文字列を取り除く
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text))
    {
      return 'n-a';
    }
    $text = preg_replace('/\W+/', '-', $text);
    $text = strtolower(trim($text, '-'));
    return $text;
  }
}
