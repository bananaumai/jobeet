<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new JobeetTestFunctional(new sfBrowser());
$browser->loadData();

$browser->info('1 - The category page')->
  info(' 1.1 - Categories on homepage are clickable')->
  get('/en/')->
  click('Programming')->
  with('request')->begin()->
    isParameter('module', 'sfJobeetCategory')->
    isParameter('action', 'show')->
    isParameter('slug', 'programming')->
  end()->

  info(' 1.2 - Categories with more than %s jobs also have a "more" link', sfConfig::get('app_max_jobs_on_homepage'))->
  get('/en/')->
  click('.more_jobs > a')->
  with('request')->begin()->
    isParameter('module', 'sfJobeetCategory')->
    isParameter('action', 'show')->
    isParameter('slug', 'programming')->
  end()->

  info(sprintf('  1.3 - Only %s jobs are listed', sfConfig::get('app_max_jobs_on_category')))->
  with('response')->checkElement('.jobs tr', sfConfig::get('app_max_jobs_on_category'))->

  info(' 1.4 - The job listed paginated')->
  with('response')->begin()->
    checkElement('.pagination_desc',sprintf('/%d jobs/', count_active_jobs_in("Programming")))->
    checkElement('.pagination_desc', sprintf('#page 1/%d#', total_page("Programming")))->
  end()->
  click('2')->
  with('request')->begin()->
    isParameter('page', 2)->
  end()->
  with('response')->checkElement('.pagination_desc', sprintf('#page 2/%d#', total_page("Programming")))
;


function count_active_jobs_in($category_name)
{
  return JobeetCategoryPeer::getForSlug(Jobeet::slugify($category_name))->countActiveJobs();
}

function total_page($category_name)
{
  return ceil(count_active_jobs_in($category_name) / sfConfig::get('app_max_jobs_on_category'));
}
